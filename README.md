# Rain-collector
Service calculates amount of collected water for any
landscapes.

#### Algorithm
1. For each landscape element calculate maximum hill from the left and from the right in parallel threads.
2. For each landscape element calculate collected amount of water using formula Min(MaxHillFromLeft,MaxHillFromRight)-elementHeight
3. Summarise all collected water (not negative values from step 2). 

#### Algorithm complexity
My solution has complexity O(3*N), if drop the non-dominant terms, complexity becomes O(N).

#### Running the tests
1. Download or clone the project.
2. Change to the root project directory.
3. Run tests using maven:
```bash
mvn test
```