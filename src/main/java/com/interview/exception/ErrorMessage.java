package com.interview.exception;

import static com.interview.Validation.MAX_FIELD_SIZE;
import static com.interview.Validation.MAX_HILL_HEIGHT;

/**
 * Custom error messages
 *
 * @author d.mikheev on 08.06.19
 */
public enum ErrorMessage {

    INVALID_HILL_HEIGHT("Height of the hill %s invalid, expected " + MAX_HILL_HEIGHT),
    INVALID_FIELD_SIZE("Size of the field %s invalid, expected " + MAX_FIELD_SIZE);

    private final String errorTextTemplate;

    ErrorMessage(final String errorTextTemplate) {
        this.errorTextTemplate = errorTextTemplate;
    }

    @Override
    public String toString() {
        return errorTextTemplate;
    }

}
