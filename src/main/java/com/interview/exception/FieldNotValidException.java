package com.interview.exception;

/**
 * Special exception for handle invalid landscapes
 *
 * @author d.mikheev on 08.06.19
 */
public class FieldNotValidException extends RuntimeException {

    public FieldNotValidException(String message) {
        super(message);
    }

    public FieldNotValidException(ErrorMessage errorMessage, String[] args) {
        super(String.format(errorMessage.toString(), args));
    }

}
