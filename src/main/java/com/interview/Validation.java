package com.interview;

/**
 * Landscape parameters validation
 *
 * @author d.mikheev on 08.06.19
 */
public class Validation {
    public static final int MAX_HILL_HEIGHT = 32000;
    public static final int MAX_FIELD_SIZE = 32000;
    private static final int TRIVIAL_ELEMENTS_QTY = 3;

    /**
     * Field size validation
     *
     * @param field input landscape
     * @return true if field is invalid
     */
    public static boolean fieldSizeNotValid(int[] field){
        if (field.length<=MAX_FIELD_SIZE&&field.length>0)
            return false;
        return true;
    }

    /**
     * Hill height validation
     *
     * @param hillHeight height of the hill
     * @return true if height is invalid
     */
    public static boolean hillHeightNotValid(int hillHeight){
        if (hillHeight<=MAX_HILL_HEIGHT&&hillHeight>=0)
            return false;
        return true;
    }

    /**
     * Check if landscape has trivial size
     *
     * @param field input landscape
     * @return true if landscape is trivial
     */
    public static boolean isTrivialLandscape(int[] field){
        if (field.length<TRIVIAL_ELEMENTS_QTY)
            return true;
        return false;
    }
}
