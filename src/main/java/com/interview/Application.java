package com.interview;

import java.io.IOException;

/**
 * Application entry class
 *
 * @author d.mikheev on 08.06.19
 */
public class Application {

    public static void main(String[] args) throws IOException {
        Application application = new Application();
        System.out.println("Rain collector started");
        application.run();
        System.in.read();
    }

    public void run() {
  }

    /**
     * Entry method wrapper
     *
     * @param landscape input landscape
     * @return
     */
     long calculateWaterAmount(int[] landscape) {
        return Core.getInstance().calculateWaterAmount(landscape);
    }

}