package com.interview;

import com.interview.exception.FieldNotValidException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static com.interview.Validation.*;
import static com.interview.exception.ErrorMessage.INVALID_FIELD_SIZE;
import static com.interview.exception.ErrorMessage.INVALID_HILL_HEIGHT;

/**
 * Core class for water calculation
 *
 * @author d.mikheev on 08.06.19
 */
public class Core {
    private static final int THREADS_QTY = Runtime.getRuntime().availableProcessors() * 2; // Threads quantity for processors with hyper threading function
    private static Core instance = null;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(THREADS_QTY);

    private Core() {
    }

    public static Core getInstance() {
        if (instance == null)
            synchronized (Core.class) {
                if (instance == null)
                    instance = new Core();
            }
        return instance;
    }

    /**
     * Completable future for calculating maximum hill for each element from right side
     */
    public CompletableFuture<Integer[]> calcRightMaximumsFuture(int[] landscape) {
        Supplier<Integer[]> rightMaximum = () -> {
            Integer[] rightMax = new Integer[landscape.length];
            int lastLandscapeIndex = landscape.length - 1;
            rightMax[lastLandscapeIndex] = landscape[lastLandscapeIndex];

            if (hillHeightNotValid(landscape[lastLandscapeIndex]))
                throw new FieldNotValidException(INVALID_HILL_HEIGHT, new String[]{String.valueOf(landscape[lastLandscapeIndex])});

            for (int i = lastLandscapeIndex; i > 0; i--) {
                if (hillHeightNotValid(landscape[i-1]))
                    throw new FieldNotValidException(INVALID_HILL_HEIGHT, new String[]{String.valueOf(landscape[i])});

                if (rightMax[i] < landscape[i - 1])
                    rightMax[i - 1] = landscape[i - 1];
                else
                    rightMax[i - 1] = rightMax[i];
            }
            return rightMax;
        };
        return CompletableFuture.supplyAsync(rightMaximum, executor);
    }

    /**
     * Completable future for calculating maximum hill for each element from the left side
     */
    public CompletableFuture<Integer[]> calcLeftMaximumsFuture(int[] landscape) {
        Supplier<Integer[]> leftMaximum = () -> {
            Integer[] leftMax = new Integer[landscape.length];
            leftMax[0] = landscape[0];

            if (hillHeightNotValid(landscape[0]))
                throw new FieldNotValidException(INVALID_HILL_HEIGHT, new String[]{String.valueOf(landscape[0])});

            for (int i = 1; i < landscape.length; i++) {
                if (hillHeightNotValid(landscape[i]))
                    throw new FieldNotValidException(INVALID_HILL_HEIGHT, new String[]{String.valueOf(landscape[i])});

                if (leftMax[i - 1] < landscape[i])
                    leftMax[i] = landscape[i];
                else
                    leftMax[i] = leftMax[i - 1];
            }
            return leftMax;
        };

        return CompletableFuture.supplyAsync(leftMaximum, executor);
    }

    /**
     * Concurrent processing landscape field
     *
     * @param landscape input field
     * @return amount of collected rain water
     */
    private long calculateNotTrivialLandscape(int[] landscape) {
        try {
            CompletableFuture<Integer[]> calcLeftMaximums = calcLeftMaximumsFuture(landscape);
            CompletableFuture<Integer[]> calcRightMaximums = calcRightMaximumsFuture(landscape);

            CompletableFuture<Long> combinedFuture = calcLeftMaximums
                    .thenCombine(calcRightMaximums, (leftMaximums, rightMaximums) -> {

                        return Stream.iterate(0, n -> n + 1)
                                .limit(landscape.length)
                                .map(e -> (Integer) (Math.min(leftMaximums[e], rightMaximums[e]) - landscape[e]))
                                .filter(e -> (e > 0))
                                .mapToLong(Integer::longValue)
                                .sum();
                    });

            return combinedFuture.get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof FieldNotValidException)
                throw new FieldNotValidException(e.getCause().getMessage());
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Method for assembly all landscape cases together
     *
     * @param landscape
     * @return amount of collected rain water
     */
    long calculateWaterAmount(int[] landscape) {

        if (fieldSizeNotValid(landscape))
            throw new FieldNotValidException(INVALID_FIELD_SIZE, new String[]{String.valueOf(landscape.length)});

        if (isTrivialLandscape(landscape))
            return 0;

        return calculateNotTrivialLandscape(landscape);
    }


}
