package com.interview;


import org.junit.Assert;
import org.junit.Test;

/**
 * Validation class unit tests
 *
 * @author d.mikheev on 08.06.19
 */
public class ValidationTest {

    @Test
    public void method_fieldSizeNotValid_whenFieldSizeNotValid_thenTrue(){
        Assert.assertEquals(true,Validation.fieldSizeNotValid(new int[100000]));
        Assert.assertEquals(true,Validation.fieldSizeNotValid(new int[32001]));
        Assert.assertEquals(true,Validation.fieldSizeNotValid(new int[0]));
    }

    @Test
    public void method_fieldSizeNotValid_whenFieldSizeIsValid_thenFalse(){
        Assert.assertEquals(false,Validation.fieldSizeNotValid(new int[32000]));
        Assert.assertEquals(false,Validation.fieldSizeNotValid(new int[1500]));
        Assert.assertEquals(false,Validation.fieldSizeNotValid(new int[1]));
    }

    @Test
    public void method_hillHeightNotValid_whenHillHeightNotValid_thenTrue(){
        Assert.assertEquals(true,Validation.hillHeightNotValid(-100));
        Assert.assertEquals(true,Validation.hillHeightNotValid(-1));
        Assert.assertEquals(true,Validation.hillHeightNotValid(32001));
        Assert.assertEquals(true,Validation.hillHeightNotValid(100000));
    }

    @Test
    public void method_hillHeightNotValid_whenHillHeightIsValid_thenFalse(){
        Assert.assertEquals(false,Validation.hillHeightNotValid(0));
        Assert.assertEquals(false,Validation.hillHeightNotValid(100));
        Assert.assertEquals(false,Validation.hillHeightNotValid(15000));
        Assert.assertEquals(false,Validation.hillHeightNotValid(32000));
    }

    @Test
    public void method_isTrivialLandscape_whenLandscapeIsTrivial_thenTrue() {
        Assert.assertEquals(true,Validation.isTrivialLandscape(new int[1]));
        Assert.assertEquals(true,Validation.isTrivialLandscape(new int[2]));
    }

    @Test
    public void method_isTrivialLandscape_whenLandscapeNotTrivial_thenFalse() {
        Assert.assertEquals(false,Validation.isTrivialLandscape(new int[3]));
        Assert.assertEquals(false,Validation.isTrivialLandscape(new int[100]));
        Assert.assertEquals(false,Validation.isTrivialLandscape(new int[10000]));
    }

}
