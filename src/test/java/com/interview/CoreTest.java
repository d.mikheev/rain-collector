package com.interview;

import com.interview.exception.FieldNotValidException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Core class unit and stress tests
 *
 * @author d.mikheev on 09.06.19
 */
public class CoreTest {

    private Core core = Core.getInstance();

    @Test(expected = FieldNotValidException.class)
    public void method_calcRightMaximumsFuture_whenHillHeightIsInvalid_thenFieldNotValidException_test1() throws InterruptedException {
        exceptionExtractor(core.calcRightMaximumsFuture(new int[]{5, 2, 3, 4, 5, 8, 0, 3, 32003}));
    }

    @Test(expected = FieldNotValidException.class)
    public void method_calcRightMaximumsFuture_whenHillHeightIsInvalid_thenFieldNotValidException_test2() throws InterruptedException {
        exceptionExtractor(core.calcRightMaximumsFuture(new int[]{5, 2, 3, 4, 32001, 4, 0, 3, 1}));
    }

    @Test(expected = FieldNotValidException.class)
    public void method_calcRightMaximumsFuture_whenHillHeightIsInvalid_thenFieldNotValidException_test3() throws InterruptedException {
        exceptionExtractor(core.calcRightMaximumsFuture(new int[]{-1, 2, 3, 4, 8, 4, 0, 3, 0}));
    }

    @Test
    public void method_calcRightMaximumsFuture_possitiveCases() throws ExecutionException, InterruptedException {
        Assert.assertArrayEquals(new int[]{5, 5, 5, 5, 5, 4, 3, 3, 1},
                Arrays.stream(core.calcRightMaximumsFuture(new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1})
                        .get()).mapToInt(Integer::intValue).toArray());

        Assert.assertArrayEquals(new int[]{4, 4, 4, 4, 3, 3, 3, 3, 3, 2},
                Arrays.stream(core.calcRightMaximumsFuture(new int[]{1, 3, 2, 4, 1, 2, 3, 2, 3, 2})
                        .get()).mapToInt(Integer::intValue).toArray());

    }

    @Test(expected = FieldNotValidException.class)
    public void method_calcLeftMaximumsFuture_whenHillHeightIsInvalid_thenFieldNotValidException_test1() throws InterruptedException {
        exceptionExtractor(core.calcLeftMaximumsFuture(new int[]{-8, 2, 3, 4, 5, 2, 0, 3, 1}));
    }

    @Test(expected = FieldNotValidException.class)
    public void method_calcLeftMaximumsFuture_whenHillHeightIsInvalid_thenFieldNotValidException_test2() throws  InterruptedException {
        exceptionExtractor(core.calcLeftMaximumsFuture(new int[]{5, 2, 3, 4, 3, 32001, 0, 3, 1}));
    }

    @Test(expected = FieldNotValidException.class)
    public void method_calcLeftMaximumsFuture_whenHillHeightIsInvalid_thenFieldNotValidException_test3() throws InterruptedException {
        exceptionExtractor(core.calcLeftMaximumsFuture(new int[]{5, 2, 3, 4, 3, 5, 0, 3, 32003}));
    }

    @Test
    public void method_calcLeftMaximumsFuture_possitiveCases() throws ExecutionException, InterruptedException {
        Assert.assertArrayEquals(new int[]{5, 5, 5, 5, 5, 5, 5, 5, 5},
                Arrays.stream(core.calcLeftMaximumsFuture(new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1})
                        .get()).mapToInt(Integer::intValue).toArray());

        Assert.assertArrayEquals(new int[]{1, 3, 3, 4, 4, 4, 4, 4, 4, 4},
                Arrays.stream(core.calcLeftMaximumsFuture(new int[]{1, 3, 2, 4, 1, 2, 3, 2, 3, 2})
                        .get()).mapToInt(Integer::intValue).toArray());

    }

    private void exceptionExtractor(CompletableFuture completableFuture) throws InterruptedException {
        try {
            completableFuture.get();
        } catch (ExecutionException e) {
            if (e.getCause() instanceof FieldNotValidException)
                throw new FieldNotValidException(e.getCause().getMessage());
        }
    }

    @Test(expected = FieldNotValidException.class)
    public void method_calculateWaterAmount_whenFieldSizeIsInvalid_thenFieldNotValidException_test1() {
        core.calculateWaterAmount(new int[0]);
    }

    @Test(expected = FieldNotValidException.class)
    public void method_calculateWaterAmount_whenFieldSizeIsInvalid_thenFieldNotValidException_test2() {
        core.calculateWaterAmount(new int[32001]);
    }

    @Test
    public void method_calculateWaterAmount_whenFiledIsTrivial_then0() {
        Assert.assertEquals(0, core.calculateWaterAmount(new int[]{150}));
        Assert.assertEquals(0, core.calculateWaterAmount(new int[]{150, 2000}));
    }

    @Test
    public void method_calculateWaterAmount_positiveZebraStressTest() {
        Function<Integer, Integer> maximizeEven = (Integer e) -> {
            int odd = e % 2;
            if (odd == 0)
                return 32000;
            return 0;
        };

        int[] zebraLandscape = Stream.iterate(0, n -> n + 1)
                .limit(32000)
                .map(maximizeEven)
                .mapToInt(Integer::intValue)
                .toArray();

        Assert.assertEquals(511968000l, core.calculateWaterAmount(zebraLandscape));
    }

    @Test
    public void method_calculateWaterAmount_positiveMaxWaterValueTest() {
        int[] maxWaterLandscape = Stream.iterate(0, n -> n + 1)
                .limit(32000)
                .map(e -> 0)
                .mapToInt(Integer::intValue)
                .toArray();

        maxWaterLandscape[0] = 32000;
        maxWaterLandscape[31999] = 32000;

        Assert.assertEquals(1023936000l, core.calculateWaterAmount(maxWaterLandscape));
    }
}
